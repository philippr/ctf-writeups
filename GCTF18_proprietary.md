# Proprietary Format 326p

The villains are communicating with their own proprietary file format. Figure out what it is.
$ nc proprietary.ctfcompetition.com 1337

[Attachment]
flag.ctf

## TL;DR;
- Server expects P6 (Netpbm) input file, does some magic and returns GCTF image.
- The image is parsed recursevly into quarters each describing one color. 
- A bit value of a command byte describes in what quarters the image gets divided. (0001 topleft, 0010 topright, 0100 bottomleft, 1000 bottomright).

## Full journey
### Get started

What do we have?
flag.ctf
> $ file flag.ctf
> flag.ctf: data

> $ hexdump -C flag.ctf
> 00000000  47 43 54 46 58 02 00 00  90 01 00 00 03 00 00 00  |GCTFX...........|
> 00000010  0f 0f 0f 0f 0f 0f 04 cb  49 ca 0d cc 47 c9 04 cc  |........I...G...|
> ...

Okay, so the first 4 bytes are the magic of the fileformat. Followed by two 4 byte numbers 0x00000258 (600) and 0x00000190 (400).
The rest of the data is a big bunch of gibberish.

Let's connect to the server:
> $ nc proprietary.ctfcompetition.com 1337
> 
> Expected P6 as the header

Don't know what P6 is but here you go:
> $ nc proprietary.ctfcompetition.com 1337
> P6
> 
> Expected width and height

> $ nc proprietary.ctfcompetition.com 1337
> P6
> 1 1
> Expected 255

> $ nc proprietary.ctfcompetition.com 1337
> P6  
> 1 1
> 255
> 
>
> 
> GCTF {and some nonprintable characters}

So the server expects some image file, lets google p6:
- It's the Netpbm format
- P6 is the Portable PixMap with 0–255 (RGB) colors 
[https://en.wikipedia.org/wiki/Netpbm_format]

### Fileformat

So the server accepts following:
> P6
> width height
> 255 (max Value)
> pixel data (r g b)

Now we need to analyse the output file format.
Let's try some more input:
> $ nc proprietary.ctfcompetition.com 1337 | xxd
> P6
> 1 1
> 255
> 1 2 3
> 00000000: 4743 5446 0100 0000 0100 0000 0032 2031  GCTF.........2 1

There is the same magic number, the dimensions we've entered, but why is the rest 0x32 0x20 0x31? Where is the 3? 
It actually took us quite some time and lot's of experiments to realize that P6 expects binary data, eventhough it is clearly marked in the wikipedia article... But that explains why the output is the hex representation of the ascii (1 space 2) input.
But once we've got there, we made some nice test images to explore the format a little more.
Here's some python code, numpy was very handy with this.

```python
import socket
import numpy as np
HOST = 'proprietary.ctfcompetition.com' 
PORT = 1337
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
width = 256
height = 256
header = f'P6\n{width} {height}\n255\n'.encode()
image = np.zeros((width, height, 3), dtype=np.uint8)
image = image.swapaxes(0,1).ravel()
req = header + bytes(image)
s.sendall(req)
data = s.recv(1024)
s.close()
with open('output', 'wb') as f:
        f.write(data)
with open('input', 'wb') as f:
        f.write(req)

```

So a 256*256 all zeros image returns this:
> 00000000: 4743 5446 0001 0000 0001 0000 0000 0000  GCTF............

and all 0xff tihs:
> 00000000: 4743 5446 0001 0000 0001 0000 00ff ffff  GCTF............

it seems that our image get's somehow compressed.

Let's create some patterns [(test Images)](https://imgur.com/a/9LiagKg)
all 0xff, bottomright corner 0x42:
> 00000000: 4743 5446 0001 0000 0001 0000 08ff ffff  GCTF............
> 00000010: 0042 4242                                .BBB

notice the 08 before the ff ff ff? There is also another 0 bye before 42 42 42.

Half 0xff half 0x42:
> 00000000: 4743 5446 0001 0000 0001 0000 0aff ffff  GCTF............
> 00000010: 0042 4242 0042 4242                      .BBB.BBB

now it's 0a and the 42 pixel is twice

let's change one more color on that half
> 00000000: 4743 5446 0001 0000 0001 0000 0aff ffff  GCTF............
> 00000010: 0042 4242 0011 1111                      .BBB....

ah so it actually split's the second half also in two parts.

How about this theory?: Our image seems to be parsed in halfs/quarter recursevly until there is only one color left and then assigns it to that subimage. 
To capture all of the 'command' bytes, that decide what way the image is devided, we used lots of more test images and came up with following syntax:

> 0 RR GG BB : one color
> 01 RR GG BB <subimage> : subimage in quarter top left, rest is the given color
> 02,04, 08 vice versa (topright, bottomleft. bottomright)
> 0c R1 G1 B1 00 R2 G2 B2 00 R3 G3 B3 : half RGB1 other half RGB2 and RGB3 
> ...

If you look at the bit representation of the command byte you might notice that the quarter commands 01,02,04,08 represent one bit each. 

> | 0001 | 0010 |
> | 0100 | 1000 |

so each bit of the command byte represents one subimage.

By playing around with the image dimensions we noticed, that rectangles are parsed by the next higher power of 2. The image get's splited and some 0 padding is added. 
From there on we had enough insight to write a parser for the flag.

### Parse the Flag

There might be much more elegant ways to parse the subimages, but we choose a simple but working approach.
See more python code:

```
import numpy as np

image = np.zeros((1024, 1024, 3), dtype=np.uint8)

def writeHeader(f, image):
        f.write("P6".encode())        
        width = image.shape[0]
        height = image.shape[1]
        f.write(("\n"+str(width)+" "+str(height)+"\n255\n").encode())
        
def divide(f, image):       
        a = f.read(1)[0]
        size = image.shape
        if(a != 0x0f):
            image[:,:,:] = list(f.read(3))
        if(a & 1):
                divide(f, image[0:size[0]//2, 0:size[1]//2,:])
        if(a & 2):
                divide(f, image[size[0]//2:, 0:size[1]//2,:])
        if(a & 4):
                divide(f, image[0:size[0]//2, size[1]//2:,:])
        if(a & 8):
                divide(f, image[size[0]//2:, size[1]//2:,:])
      
with open('flag.ctf', 'rb') as f:
        header = f.read(4);
        width = f.read(4);
        height = f.read(4);
        divide(f,image)

with open('parsedOutput', 'wb') as f:
        writeHeader(f, image)
        image = image.swapaxes(0,1).ravel()
        f.write(image)
```

Finnaly you receive a beautiful [flag image](https://imgur.com/a/HtEBX04).


